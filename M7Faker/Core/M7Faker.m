//
//  M7Faker.m
//  M7Faker
//
//  Created by Rafał Wójcik on 25.03.2014.
//  Copyright (c) 2014 CodeFlex. All rights reserved.
//

#import "M7Faker.h"
#import <CoreMotion/CoreMotion.h>
#import "M7CMMotionActivity.h"

@interface M7Faker()

@property (nonatomic, strong, readwrite) NSString *JSONActivitiesPath;
@property (nonatomic, strong, readwrite) NSArray *activities;

@end

@implementation M7Faker

+ (M7Faker *)shared {
    static M7Faker *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[M7Faker alloc] init];
    });
    return _sharedInstance;
}

- (void)setupFakeDataWithJSONPath:(NSString *)path {
    
    NSString *jsonString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    
    if (jsonString) {
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSDictionary *parsedJson = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];
        
        NSArray *parsedData = parsedJson[@"d"];
        
        [self calculateBaseDateWithTimeInterval:(NSNumber *)parsedJson[@"t"]];
        
        NSMutableArray *activitiesTempArray = [[NSMutableArray alloc] initWithCapacity:[parsedData count]];
        
        [parsedData enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            if ([obj isKindOfClass:[NSDictionary class]]) {
                
                CMMotionActivity *activity = (CMMotionActivity *)[[M7CMMotionActivity alloc] initWithDictionary:(NSDictionary *)obj andBaseDate:self.baseDate];
                
                [activitiesTempArray addObject:activity];
                
            } else {
                NSLog(@"Invalid JSON structure");
                return ;
            }
        }];
        self.activities = [activitiesTempArray copy];
    } else {
        NSLog(@"Some thing goes wrong while reading json file !!!");
    }
}

- (void)setupFakeDataWithExampleJSON {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"exampleM7DataDump" ofType:@"json"];
    [self setupFakeDataWithJSONPath:path];
}

-(void)calculateBaseDateWithTimeInterval:(NSNumber *)timeIntervalNumber {
    if (!self.baseDate) {
        NSTimeInterval timestamp = [timeIntervalNumber doubleValue];
        self.baseDate = [NSDate dateWithTimeIntervalSince1970:timestamp];
    }
    if (self.shoudMoveDataToToday) {
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        NSDateComponents *baseDateComponents = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSTimeZoneCalendarUnit) fromDate:self.baseDate];
        NSDateComponents *todayDateComponents = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate dateWithTimeIntervalSinceNow:0]];
        
        [baseDateComponents setYear:todayDateComponents.year];
        [baseDateComponents setMonth:todayDateComponents.month];
        [baseDateComponents setDay:todayDateComponents.day];
        
        self.baseDate = [calendar dateFromComponents:baseDateComponents];
    }
}

-(id)init {
    self = [super init];
    if (self) {
        self.activities = [[NSArray alloc] init];
        self.shoudMoveDataToToday = NO;
        self.baseDate = nil;
    }
    return self;
}

@end

