//
//  M7CMMotionActivity.m
//  M7Faker
//
//  Created by Rafał Wójcik on 25.03.2014.
//  Copyright (c) 2014 Softhis. All rights reserved.
//

#import "M7CMMotionActivity.h"

@interface M7CMMotionActivity()

@property (assign, readwrite) NSUInteger steps;

@property (assign, readwrite) CMMotionActivityConfidence confidence;
@property (assign, readwrite) BOOL stationary;
@property (assign, readwrite) BOOL walking;
@property (assign, readwrite) BOOL running;
@property (assign, readwrite) BOOL automotive;
@property (assign, readwrite) BOOL unknown;
@property (nonatomic, readwrite) NSDate *startDate;

@end

@implementation M7CMMotionActivity

@synthesize confidence = _confidence;
@synthesize stationary = _stationary;
@synthesize walking = _walking;
@synthesize running = _running;
@synthesize automotive = _automotive;
@synthesize unknown = _unknown;
@synthesize startDate = _startDate;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary andBaseDate:(NSDate *)baseDate {
    self = [super init];
    if (self) {
        _steps = [(NSNumber *)[dictionary objectForKey:@"t"] integerValue];
        
        _confidence = [(NSNumber *)[dictionary objectForKey:@"c"] integerValue];
        _stationary = [(NSNumber *)[dictionary objectForKey:@"s"] boolValue];
        _walking = [(NSNumber *)[dictionary objectForKey:@"w"] boolValue];
        _running = [(NSNumber *)[dictionary objectForKey:@"r"] boolValue];
        _automotive = [(NSNumber *)[dictionary objectForKey:@"a"] boolValue];
        _unknown = [(NSNumber *)[dictionary objectForKey:@"u"] boolValue];
        
        NSNumber *dateInterval = (NSNumber *)[dictionary objectForKey:@"i"];
        _startDate = [baseDate dateByAddingTimeInterval:[dateInterval integerValue]];
    }
    return self;
}

- (NSString *)description {
    
    NSString *confidence = @"LOW";
    if (self.confidence == CMMotionActivityConfidenceMedium) confidence = @"MEDIUM";
    if (self.confidence == CMMotionActivityConfidenceHigh) confidence = @"HIGH";
    
    NSString *stationary = @"NO";
    if (self.stationary) stationary = @"YES";
    
    NSString *walking = @"NO";
    if (self.walking) walking = @"YES";
    
    NSString *running = @"NO";
    if (self.running) running = @"YES";
    
    NSString *automotive = @"NO";
    if (self.automotive) automotive = @"YES";
    
    NSString *unknown = @"NO";
    if (self.unknown) unknown = @"YES";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ssZ"];
    
    return [NSString stringWithFormat:@"<CMMotionActivity:%p>\nConfidence: %@\nStationary: %@\nWalking: %@\nRunning: %@\nAutomotive: %@\nUnknow: %@\nStart date: %@", self, confidence, stationary, walking, running, automotive, unknown, [dateFormatter stringFromDate:self.startDate]];
}

- (NSString *)activityDescription {
    if (self.stationary) return @"Stationary";
    if (self.walking) return @"Walking";
    if (self.running) return @"Running";
    if (self.automotive) return @"Automotive";
    if (self.unknown) return @"Unknow";
    return @"Unknow";
}

- (NSString *)shortDescription {
    
    NSString *confidence = @"LOW";
    if (self.confidence == CMMotionActivityConfidenceMedium) confidence = @"MEDIUM";
    if (self.confidence == CMMotionActivityConfidenceHigh) confidence = @"HIGH";
    
    NSString *activity = [self activityDescription];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    return [NSString stringWithFormat:@"%@ -> %@ -> %@ (Steps: %lu)", [dateFormatter stringFromDate:self.startDate], activity, confidence, (unsigned long)self.steps];
}

@end
