Pod::Spec.new do |s|
  s.name             = "M7Faker"
  s.version          = "0.1.0"
  s.summary          = ""
  
  s.homepage         = ""
  s.screenshots      = ""
  s.license          = 'MIT'
  s.author           = { "Rafał Wójcik" => "rafalwojcik@me.com" }
  s.source           = { :git => "https://bitbucket.org/m7faker/library.git", :branch => "master" }

  s.platform     = :ios, '7.0'
  s.ios.deployment_target = '7.0'
  s.requires_arc = true

  s.source_files = "M7Faker/**/*.{h,m}"
  s.header_dir = 'M7Faker'
  s.public_header_files = 'M7Faker/**/*.h'
  s.resource = "M7Faker/exampleM7DataDump.json"
  
  s.prefix_header_contents = '#import <CoreMotion/CoreMotion.h>'
end
