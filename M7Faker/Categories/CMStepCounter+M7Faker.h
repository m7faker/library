//
//  CMStepCounter+M7Faker.h
//  M7Faker
//
//  Created by Rafał Wójcik on 26.03.2014.
//  Copyright (c) 2014 CodeFlex. All rights reserved.
//

#import <CoreMotion/CoreMotion.h>

@interface CMStepCounter (M7Faker)

+ (BOOL)M7_isStepCountingAvailable;

- (void)M7_queryStepCountStartingFrom:(NSDate *)start to:(NSDate *)end toQueue:(NSOperationQueue *)queue withHandler:(CMStepQueryHandler)handler;

@end
