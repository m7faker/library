# M7Faker

[![Version](http://cocoapod-badges.herokuapp.com/v/M7Faker/badge.png)](http://cocoadocs.org/docsets/M7Faker)
[![Platform](http://cocoapod-badges.herokuapp.com/p/M7Faker/badge.png)](http://cocoadocs.org/docsets/M7Faker)

## Usage

To run the example project; clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

M7Faker is available through [CocoaPods](http://cocoapods.org), to install
it simply add the following line to your Podfile:

    pod "M7Faker"

## Author

Rafał Wójcik, rw@softhis.com

## License

M7Faker is available under the MIT license. See the LICENSE file for more info.

