//
//  CMMotionActivityManager+M7Faker.h
//  M7Faker
//
//  Created by Rafał Wójcik on 25.03.2014.
//  Copyright (c) 2014 Softhis. All rights reserved.
//

#import <CoreMotion/CoreMotion.h>

@interface CMMotionActivityManager (M7Faker)

+ (BOOL)M7_isActivityAvailable;

- (void)M7_queryActivityStartingFromDate:(NSDate *)start toDate:(NSDate *)end toQueue:(NSOperationQueue *)queue withHandler:(CMMotionActivityQueryHandler)handler;

@end
