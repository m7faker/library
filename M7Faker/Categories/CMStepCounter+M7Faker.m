//
//  CMStepCounter+M7Faker.m
//  M7Faker
//
//  Created by Rafał Wójcik on 26.03.2014.
//  Copyright (c) 2014 CodeFlex. All rights reserved.
//

#import "CMStepCounter+M7Faker.h"
#import "M7Faker.h"

@implementation CMStepCounter (M7Faker)

+ (BOOL)M7_isStepCountingAvailable {
    return YES;
}

- (void)M7_queryStepCountStartingFrom:(NSDate *)start to:(NSDate *)end toQueue:(NSOperationQueue *)queue withHandler:(CMStepQueryHandler)handler {
    [queue addOperationWithBlock:^{
        
        NSPredicate *datePeriodPredicate = [NSPredicate predicateWithFormat:@"startDate >= %@ AND startDate <= %@", start, end];
        
        NSArray *activities = [[M7Faker shared].activities filteredArrayUsingPredicate:datePeriodPredicate];
        
        NSUInteger stepsCount = [[activities valueForKeyPath: @"@sum.steps"] integerValue];
        
        if(handler) handler(stepsCount, nil);
    }];
}

@end
