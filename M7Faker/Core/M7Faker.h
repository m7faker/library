//
//  M7Faker.h
//  M7Faker
//
//  Created by Rafał Wójcik on 25.03.2014.
//  Copyright (c) 2014 CodeFlex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface M7Faker : NSObject

@property (nonatomic, strong, readonly) NSArray *activities;
@property (nonatomic, strong) NSDate *baseDate;
@property (assign) BOOL shoudMoveDataToToday;

+ (M7Faker *)shared;

- (void)setupFakeDataWithJSONPath:(NSString *)path;

- (void)setupFakeDataWithExampleJSON;

@end
