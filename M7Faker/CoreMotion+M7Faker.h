//
//  CoreMotion+M7Faker.h
//  M7Faker
//
//  Created by Rafał Wójcik on 25.03.2014.
//  Copyright (c) 2014 Softhis. All rights reserved.
//

#ifdef __OBJC__

    #import <CoreMotion/CoreMotion.h>
    #import "M7CMMotionActivity.h"
    #import "M7Faker.h"
    #import "CMMotionActivityManager+M7Faker.h"
    #import "CMStepCounter+M7Faker.h"

#endif