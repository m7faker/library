//
//  main.m
//  M7FakerExample
//
//  Created by Rafał Wójcik on 25.03.2014.
//  Copyright (c) 2014 CodeFlex. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
