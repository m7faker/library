//
//  AppDelegate.h
//  M7FakerExample
//
//  Created by Rafał Wójcik on 25.03.2014.
//  Copyright (c) 2014 CodeFlex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
