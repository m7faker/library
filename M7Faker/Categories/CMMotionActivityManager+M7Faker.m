//
//  CMMotionActivityManager+M7Faker.m
//  M7Faker
//
//  Created by Rafał Wójcik on 25.03.2014.
//  Copyright (c) 2014 Softhis. All rights reserved.
//

#import "CMMotionActivityManager+M7Faker.h"
#import "M7Faker.h"

@implementation CMMotionActivityManager (M7Faker)

+ (BOOL)M7_isActivityAvailable {
    return YES;
}

- (void)M7_queryActivityStartingFromDate:(NSDate *)start toDate:(NSDate *)end toQueue:(NSOperationQueue *)queue withHandler:(CMMotionActivityQueryHandler)handler {
    [queue addOperationWithBlock:^{
        NSArray *activities = [M7Faker shared].activities;
        
        NSPredicate *datePeriodPredicate = [NSPredicate predicateWithFormat:@"startDate >= %@ AND startDate <= %@", start, end];
        
        if(handler) handler([activities filteredArrayUsingPredicate:datePeriodPredicate] ,nil);
    }];
}

@end
