//
//  ViewController.m
//  M7FakerExample
//
//  Created by Rafał Wójcik on 25.03.2014.
//  Copyright (c) 2014 CodeFlex. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *activitiesCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *stepsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *M7ChipAvailableLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateStartLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateEndLabel;
@property (weak, nonatomic) IBOutlet UITextView *activitiesTextfield;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, strong) CMMotionActivityManager *manager;
@property (nonatomic, strong) CMStepCounter *stepCounter;

@end

@implementation ViewController

- (void)viewDidLoad
{
    self.manager = nil;
    self.endDate = [NSDate dateWithTimeIntervalSinceNow:0];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.dateEndLabel.text = [self.dateFormatter stringFromDate:self.endDate];
    
    [[M7Faker shared] setupFakeDataWithExampleJSON];
    
    if ([CMMotionActivityManager M7_isActivityAvailable]) {
        self.M7ChipAvailableLabel.text = @"YES";
        self.manager = [[CMMotionActivityManager alloc] init];
        self.stepCounter = [[CMStepCounter alloc] init];
    }
    
    [self queryActivitiesBasedOnSliderValue:-7*24*60*60];
    [self changeLogActivitiesCount:nil];
    
    [super viewDidLoad];
}

- (IBAction)changeDate:(UISlider *)slider {
    NSInteger sliderValue = (NSInteger)slider.value;
    [self queryActivitiesBasedOnSliderValue:sliderValue];
}

- (void)queryActivitiesBasedOnSliderValue:(NSInteger)sliderValue {
    NSDate *fromDate = [NSDate dateWithTimeIntervalSinceNow:sliderValue];
    
    [self.manager M7_queryActivityStartingFromDate:fromDate toDate:self.endDate toQueue:[NSOperationQueue mainQueue] withHandler:^(NSArray *activities, NSError *error) {
        self.dateStartLabel.text = [self.dateFormatter stringFromDate:fromDate];
        self.activitiesCountLabel.text = [NSString stringWithFormat:@"%d", [activities count]];
    }];
    
    [self.stepCounter M7_queryStepCountStartingFrom:fromDate to:self.endDate toQueue:[NSOperationQueue mainQueue] withHandler:^(NSInteger numberOfSteps, NSError *error) {
        self.stepsCountLabel.text = [NSString stringWithFormat:@"%d", numberOfSteps];
    }];
}

- (IBAction)changeLogActivitiesCount:(UISegmentedControl *)segmentedControll {
    
    NSUInteger activitiesToLog = 25;
    switch (segmentedControll.selectedSegmentIndex) {
        case 1: activitiesToLog = 50; break;
        case 2: activitiesToLog = 100; break;
        case 3: activitiesToLog = 200; break;
    }
    
    NSArray *lastActivities = [[M7Faker shared].activities subarrayWithRange:NSMakeRange([[M7Faker shared].activities count] - activitiesToLog, activitiesToLog)];
    
    NSMutableString *activitiesLog = [@"" mutableCopy];
    
    [lastActivities enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(M7CMMotionActivity *activity, NSUInteger idx, BOOL *stop) {
        [activitiesLog appendString:[activity shortDescription]];
    }];
    
    [self.activitiesTextfield setText:[activitiesLog copy]];
}


@end
