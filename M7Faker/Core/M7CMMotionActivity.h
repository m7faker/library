//
//  M7CMMotionActivity.h
//  M7Faker
//
//  Created by Rafał Wójcik on 25.03.2014.
//  Copyright (c) 2014 Softhis. All rights reserved.
//

#import <CoreMotion/CoreMotion.h>

@interface M7CMMotionActivity : CMMotionActivity

@property (assign, readonly) NSUInteger steps;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary andBaseDate:(NSDate *)baseDate;

- (NSString *)activityDescription;
- (NSString *)shortDescription;

@end
